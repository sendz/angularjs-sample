(function () {
  'use strict'
  angular.module('app').directive('customTable', table)
  table.$inject = []

  function table() {
    return {
      templateUrl: './src/js/partials/directive/dashboard/table/table.html',
      controller: TableController,
      restrict: 'E',
      scope: {
        inputData: '=',
        methodToExecute: '&'
      },
      link: link
    }

    function link(scope, element, attributes) {

    }
  }


  TableController.$inject = []

  function TableController() {

  }
})()